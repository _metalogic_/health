const std = @import("std");
const heap = @import("std").heap;
const http = @import("std").http;

const Error = error{
    InvalidArgument,
    HttpError,
    ServiceUnhealthy,
};

const ok = "ok\n";

pub fn main() !void {
    const allocator = heap.page_allocator;

    // Get command-line arguments
    const args = try std.process.argsAlloc(allocator);
    defer std.process.argsFree(allocator, args);

    if (args.len != 2) {
        std.log.err("required health check URL was not provided", .{});
        return Error.InvalidArgument;
    }
    const url = args[1];

    // Create HTTP client
    var client: http.Client = .{ .allocator = allocator };

    // Send GET request
    const uri = try std.Uri.parse(url);
    var server_header_buffer: [1024]u8 = undefined;
    var req = try client.open(.GET, uri, .{
        .server_header_buffer = &server_header_buffer,
    });

    defer req.deinit();

    try req.send();
    try req.wait();

    // Read response body
    const body = try req.reader().readAllAlloc(allocator, 8192);
    defer allocator.free(body);

    // Print the body
    const stdout = std.io.getStdOut().writer();
    try stdout.print("Response body: {s}\n", .{body});

    // Check HTTP status code
    const status_code = @intFromEnum(req.response.status);
    if (status_code != 200) {
        const stderr = std.io.getStdErr().writer();
        try stderr.print("HTTP request failed with status code {}", .{status_code});
        return Error.ServiceUnhealthy;
    }
}
