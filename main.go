package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

var ok = []byte("ok\n")

func main() {

	if len(os.Args) != 2 {
		log.Fatalln("required health check URL was not provided")
	}
	url := os.Args[1]
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	if resp.StatusCode != http.StatusOK || bytes.Compare(body, ok) != 0 {
		log.Fatalf("service at '%s' is unhealthy\n", url)
	}
}
